import { Ifilter } from './../../config/support/interfaces/filter/Ifilter';
import { Icidade } from '../../config/support/interfaces/cidade/Icidade';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Service   } from '../Service';

@Injectable({
  providedIn: 'root'
})
export class CidadesService extends Service {
  constructor( protected http:HttpClient) {
          super();
  }

   getIndex(page:number=0){

     return this.http.get<Icidade>(this.url + '/cidades/page/'+page);

  }

  create(cidade:any){

    console.log(cidade);

    return this.http.post(this.url + '/cidades/create',cidade);
  }

  show(id:String){

    return this.http.get<Icidade>(this.url + '/cidades/show/'+id);

  }

  update(cidade:Icidade){

    return this.http.put(this.url + '/cidades/update',cidade);

  }

  buscar(filtro:any,page:number=0){

    return this.http.post(this.url + '/cidades/filtrar/page/'+page,filtro);

  }

  deletar(id:String){

    return this.http.delete(this.url + '/cidades/delete/'+id);
  }



}
