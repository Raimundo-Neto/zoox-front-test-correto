import { Ifilter } from './../../config/support/interfaces/filter/Ifilter';
import { Iestado } from '../../config/support/interfaces/estado/Iestado';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Service   } from '../Service';

@Injectable({
  providedIn: 'root'
})
export class EstadosService extends Service {
protected  ops  = {

  headers: new HttpHeaders ({
    'Content-Type':'application/json',


  }),
};

  constructor( protected http:HttpClient ) {
super();
  }

   getIndex(page:number=0){
     return this.http.get<Iestado>(this.url + '/estados/page/'+page);

  }

  create(cidade:any){
    let head = new HttpHeaders({    'Content-Type': 'application/x-www-form-urlencoded, charset=UTF-8'});
    return this.http.post(this.url + '/estados/create',cidade,{headers:head});
  }

  show(id:String){

    return this.http.get<Iestado>(this.url + '/estados/show/'+id);

  }

  update(cidade:any){
console.log(cidade);
    return this.http.put(this.url + '/estados/update',cidade);

  }
  buscar(filtro:any,page:number=0){

    return this.http.post(this.url + '/estados/filtrar/page/'+page,filtro);

  }
  deletar(id:String){

    return this.http.delete(this.url + '/estados/delete/'+id);
  }



}
