
import { MaterialModule } from './material/material.module';

// import { PoloniexService } from './services/poloniex/poloniex.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';

import { HttpClientModule } from '@angular/common/http';
// import { MarketplaceComponent } from './marketplace/marketplace.component';

import { FilterComponent } from './filter/filter.component';
import { ResultComponent } from './filter/result/result.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { MatSidenavContainer } from '@angular/material/sidenav';
import { CidadesService } from './services/cidades/cidades';
import { EstadosService } from './services/estados/estados';
import { HomeModule } from './home/home.module';
import { CidadesComponent } from './cidades/cidades.component';
import { CidadesEditComponent } from './cidades-edit/cidades-edit.component';
import { FormsModule } from '@angular/forms';
import { EstadosComponent } from './estados/estados.component';
import { EstadosEditComponent } from './estados-edit/estados-edit.component';
import { ngxLoadingAnimationTypes, NgxLoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    AppComponent,

    HomeComponent,
    FilterComponent,
    ResultComponent,
    HomeComponent,
    CidadesComponent,
    CidadesEditComponent,
    EstadosComponent,
    EstadosEditComponent,


//    DashboardPublisherComponent,
   // MarketplaceComponent,
  //  MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    MatSliderModule,
    MaterialModule,
    HomeModule,
    FormsModule,
    BrowserAnimationsModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.wanderingCubes,
      backdropBackgroundColour: '#59eb88',
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff',
      secondaryColour: '#ffffff',
      tertiaryColour: '#ffffff'
  }),


  ],
  providers: [
  MatSidenavContainer,
  CidadesService,
  EstadosService
  ]
    ,
  bootstrap: [AppComponent],
  exports:[]
})
export class AppModule { }
