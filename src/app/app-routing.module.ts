import { CidadesEditComponent } from './cidades-edit/cidades-edit.component';
import { CidadesComponent } from './cidades/cidades.component';


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EstadosComponent } from './estados/estados.component';
import { EstadosEditComponent } from './estados-edit/estados-edit.component';


const routes: Routes = [

 // { path: '', component: './home/home.module#HomeModule' },
  { path: 'cidades', component: CidadesComponent },
  { path: 'cidades-edit', component: CidadesEditComponent },
  { path: 'estados', component: EstadosComponent },
  { path: 'estados-edit', component: EstadosEditComponent },
  { path: 'home', component: HomeComponent },
  { path: 'cidades-edit', loadChildren: './CidadesEditComponent' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
