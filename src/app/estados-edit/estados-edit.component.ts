import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Iestado } from '../config/support/interfaces/estado/Iestado';
import { CidadesService } from '../services/cidades/cidades';
import { EstadosService } from '../services/estados/estados';

@Component({
  selector: 'app-estados-edit',
  templateUrl: './estados-edit.component.html',
  styleUrls: ['./estados-edit.component.scss']
})
export class EstadosEditComponent implements OnInit {

  public estadoSelecao;
  public estado: Iestado;
  public nome:String ;
  protected dataSend = [];
  protected id;
  public result;
  public uf;


  constructor(protected estadoService:EstadosService, protected cidadeService:CidadesService, protected navigation:Router, protected routeParams:ActivatedRoute) {

    this.estado = new Iestado();
  }



  salvar(){
    this.estado.nome = this.nome;


this.estadoService.update(  {nome:this.nome,uf:this.uf,id:this.result.data._id}   ).subscribe((resp)=>{ console.log(resp);alert('Dados Atualizados');},(error)=>{ console.log(error)});

  }

  setUf(value){

    this.uf = value;
  }


  setNome(value){

    this.nome = value;
  }



  buscaDados(id){


    this.estadoService.show(id).subscribe((resp)=>{
    console.log(resp);
    this.result = resp;
    this.nome = this.result.data.nome;
    this.uf = this.result.data.uf;

    },(error)=>{console.log('error')});



    }

    paginaAnterior(){

      this.navigation.navigate(['/']);

      }

  ngOnInit(): void {


    this.routeParams.queryParams.subscribe(par => {
      console.log('par',par['id']);
      this.id = par['id'];
      this.buscaDados(par['id']);

   });



    this.estadoService.getIndex().subscribe((resp)=>{
      this.estadoSelecao = resp;
    },(error)=>{ console.log(error)});

  }
}
