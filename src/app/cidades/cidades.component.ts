import { Router } from '@angular/router';

import { EstadosService } from './../services/estados/estados';
import { Component, OnInit } from '@angular/core';
import { Icidade } from '../config/support/interfaces/cidade/Icidade';
import { CidadesService } from '../services/cidades/cidades';

@Component({
  selector: 'app-cidades',
  templateUrl: './cidades.component.html',
  styleUrls: ['./cidades.component.scss']
})
export class CidadesComponent implements OnInit {

  public estadoSelecao;

  public cidade: Icidade;

  public nome:String ;

  protected dataSend = [];

  constructor(protected estadoService:EstadosService, protected cidadeService:CidadesService, protected route:Router) {

    this.cidade = new Icidade();
  }

  selectOption(value){
console.log('set value' + value);
this.cidade.uf = value;

  }
  setNome(value){
this.nome = value;

  }


paginaAnterior(){

this.route.navigate(['/']);

}

  salvar(){

    this.cidade.nome = this.nome;


    console.log(this.cidade);


this.cidadeService.create( {nome:this.cidade.nome,uf:this.cidade.uf,id:this.cidade.id} ).subscribe((resp)=>{ console.log(resp); alert('Dados Atualizados');},(error)=>{ console.log(error)});

  }

  ngOnInit(): void {

    this.estadoService.getIndex().subscribe((resp)=>{
      this.estadoSelecao = resp;
    },(error)=>{ console.log(error)});
  }

}
