import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';

library.add(fas,fab);


export  class  Iicon {

static ICON = {
iYoutube : findIconDefinition({ prefix: 'fab', iconName: 'youtube' }),
iGlobe : findIconDefinition({ prefix: 'fas', iconName: 'globe-americas' }),
iFacebook : findIconDefinition({ prefix: 'fab', iconName: 'facebook' }),
iFilter : findIconDefinition({ prefix: 'fas', iconName: 'sliders-h' }),
iSearch : findIconDefinition({ prefix: 'fas', iconName: 'search' }),
iEdit: findIconDefinition({prefix: 'fas', iconName: 'pen-square'}),
iDelete: findIconDefinition({prefix: 'fas', iconName: 'trash'})
};

}
