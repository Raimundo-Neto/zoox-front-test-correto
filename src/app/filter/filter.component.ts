
import { Icidade } from '../config/support/interfaces/cidade/Icidade';

import { Ifilter } from './../config/support/interfaces/filter/Ifilter';
import { Resources } from './../config/resources';
import { Iicon } from './../config/support/interfaces/icons/Iicon';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { EstadosService } from '../services/estados/estados';
import { CidadesService } from '../services/cidades/cidades';



@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export  class FilterComponent implements OnInit {



  public icons;
  public toggleFilter = false;
  public filter: Ifilter;
  public isCidadeClick = true;
  public isEstadoClick = false;
  public result: any;
  public selectForm = new FormControl();
  public selectList: any;
  public load: Boolean = false;




  constructor( protected cidadeService: CidadesService, protected estadoService: EstadosService ) {
    this.icons = Resources.ICONS;
    this.filter = new Ifilter();

   }

openFilter(){

this.toggleFilter = !this.toggleFilter;

}

resetFilter(){
delete this.filter;

this.filter = new Ifilter();


}
clickCidades(){
  this.isEstadoClick = false;
  this.isCidadeClick = true;
  this.setFilterTipoBusca('cidades');
  this.filtrarCidades();
}

clickEstados(){
  this.isCidadeClick = false;
  this.isEstadoClick = true;
  this.setFilterTipoBusca('estados');
  this.filtrarEstados();
}

setFilterTipoBusca(value: String){

this.filter.tipoBusca = value;

}

filtrarCidades(){

  this.load = true;
  this.result = this.cidadeService.buscar({nome: this.filter.nome, uf: this.filter.uf}).subscribe((resp) => {
    this.load = false;
    this.result = resp;
    console.log(resp);
  }, (erro) => {
    console.log(erro);
    this.load = false;
  });

}

filtrarEstados(){

  this.load = true;
  this.result = this.estadoService.buscar({nome: this.filter.nome, uf: this.filter.uf}).subscribe((resp) => {
    this.load = false;
    this.result = resp;
    console.log(this.result);
  },(erro) => {
    console.log(erro);
    this.load = false;
  });

}

filtrar(){
if(this.filter.tipoBusca == 'cidades'){

  this.filtrarCidades();
}

if(this.filter.tipoBusca == 'estados'){

  this.filtrarEstados();
}

}


index(){


  this.filter.tipoBusca = 'cidades';
  this.load = true;
  this.cidadeService.getIndex().subscribe((resp) => {
    this.load = false;
    this.result = resp;
    console.log(this.result);
});



}

BuscarSelect(){

  this.estadoService.getIndex().subscribe((resp) => {
    this.selectList = resp;
    console.log(resp);
  }, (erro) => {
    console.log(erro);

  });


}
  ngOnInit(): void {
    this.index();
    this.BuscarSelect();

  }



}
