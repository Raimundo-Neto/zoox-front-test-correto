
import { EstadosService } from './../../services/estados/estados';
import { Service } from './../../services/Service';
import { Ifilter } from './../../config/support/interfaces/filter/Ifilter';
import { Resources } from './../../config/resources';
import { Component, Input, OnInit } from '@angular/core';
import { Iicon } from 'src/app/config/support/interfaces/icons/Iicon';
import { CidadesService } from 'src/app/services/cidades/cidades';
import { Router } from '@angular/router';


@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {



  @Input() result: any;
  @Input() filtro: Ifilter;
  @Input() loading: Boolean;

  public icons;
  protected service;
  protected item;
  public idDeletar;


  constructor(protected estadoService:EstadosService, protected cidadeService:CidadesService, protected router:Router) {
    this.icons = Resources.ICONS;

  }

  ngOnInit(): void {

  }

  ver(data){
console.log(data);

  }




 closeModalDeletar(){

  this.idDeletar = false;

 }


abrirModalDeletar(res){
this.item = res;
this.idDeletar = true;
}

cadastrar(path:String):void{
console.log(path);
this.router.navigate(['/'+path]);

}

deletar(){

this.buscarService(this.item);
this.loading = true;
this.service.deletar(this.item._id).subscribe((resp)=>{
  this.loading = false;
  console.log(resp);
this.closeModalDeletar();

},(erro) =>{
  this.loading = false;
  console.log(erro);
  this.closeModalDeletar();
});


}
getId(res){

return res._id;

}

getRouteUrL(){
return '/' +  this.filtro.tipoBusca  + '-edit/';

}

editarItem(res){
let cod =  res.id;
this.router.navigate(['/'+this.filtro.tipoBusca + '-edit'],{ queryParams:{ id : cod}});
}

getPage(id:number){



}


buscarService(res){
  if(res.estado_id){

    this.service = this.cidadeService;
    return true;

  }else{

    this.service = this.estadoService;
}



}


}
