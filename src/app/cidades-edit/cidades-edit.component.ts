import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Icidade } from '../config/support/interfaces/cidade/Icidade';
import { CidadesService } from '../services/cidades/cidades';
import { EstadosService } from '../services/estados/estados';

@Component({
  selector: 'app-cidades-edit',
  templateUrl: './cidades-edit.component.html',
  styleUrls: ['./cidades-edit.component.scss']
})
export class CidadesEditComponent implements OnInit {


  public estadoSelecao;
  public cidade: Icidade;
  public nome:String;
  protected id;
  public uf;
 public result;

  constructor(protected estadoService:EstadosService, protected cidadeService:CidadesService, protected route:ActivatedRoute,protected navigation:Router) {

    this.cidade = new Icidade();
  }

  selectOption(value){

this.uf = value;

  }



  setNome(value){

    this.nome = value;
  }



paginaAnterior(){

this.navigation.navigate(['/']);

}

  salvar(){

    this.cidade.nome = this.nome;


this.cidadeService.update( {nome:this.nome,uf:this.uf,id:this.result.data._id} ).subscribe((resp)=>{ console.log(resp); alert('dados Atualizados com sucesso')},(error)=>{ console.log(error)});

  }

  buscaDados(id){


    this.cidadeService.show(id).subscribe((resp)=>{
    console.log(resp);
    this.result = resp;
    this.nome = this.result.data.nome;
    this.uf = this.result.data.uf;

    },(error)=>{console.log('error')});



    }



  ngOnInit(): void {

    this.route.queryParams.subscribe(par => {
      console.log('par',par['id']);
      this.id = par['id'];
      this.buscaDados(par['id']);
    });


    this.estadoService.getIndex().subscribe((resp)=>{
      this.estadoSelecao = resp;
    },(error)=>{ console.log(error)});
  }

}
