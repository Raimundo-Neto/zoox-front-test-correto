import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CidadesEditComponent } from './cidades-edit.component';

describe('CidadesEditComponent', () => {
  let component: CidadesEditComponent;
  let fixture: ComponentFixture<CidadesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CidadesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CidadesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
