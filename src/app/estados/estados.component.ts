

import { Component, OnInit } from '@angular/core';
import { EstadosService } from '../services/estados/estados';
import { CidadesService } from '../services/cidades/cidades';
import { Iestado } from '../config/support/interfaces/estado/Iestado';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-estados',
  templateUrl: './estados.component.html',
  styleUrls: ['./estados.component.scss']
})
export class EstadosComponent implements OnInit {

  public estadoSelecao;
  public estado: Iestado;
  public nome:String;
  public uf:String;
  protected dataSend = [];

  constructor(protected estadoService:EstadosService, protected cidadeService:CidadesService,protected routeParams:Router) {

    this.estado = new Iestado();
  }

  setNome(value){

    this.nome = value;
  }

  setUf(value){

    this.uf = value;
  }

  salvar(){

this.estadoService.create( {nome: this.nome, uf: this.uf } ).subscribe((resp)=>{ console.log(resp); alert('Dados Atualizados');},(error)=>{ console.log(error)});

  }
  paginaAnterior(){

    this.routeParams.navigate(['/']);

    }

  ngOnInit(): void {

    this.estadoService.getIndex().subscribe((resp)=>{
      this.estadoSelecao = resp;
    },(error)=>{ console.log(error)});
  }
}
